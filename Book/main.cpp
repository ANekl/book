#include <iostream>
#include <fstream>
#include <vector>
#include "person.h"

using namespace std;

void write_book(vector<Person> &vPerson)
{
    for(int i =0; i<vPerson.size();i++)
    {
        string const &name=vPerson[i].getName();
        string const &second_name=vPerson[i].getSecond_Name();
        string const &number=vPerson[i].getNumber();
        cout<<i<<" "<<name<<" "<<second_name<<" "<<number<<endl;
    }
    cout<<endl;
}
void ask_action()
{
    cout<<"Choose action"<<endl;
    cout<<"1. Add contact"<<endl;
    cout<<"2. Change contact"<<endl;
    cout<<"3. Delete contakt"<<endl;
    cout<<"4. Save"<<endl;
    cout<<endl;
}
void ask_change()
{
    cout<<"Choose action"<<endl;
    cout<<"1. Change Name"<<endl;
    cout<<"2. Change Second Name"<<endl;
    cout<<"3. Change Number"<<endl;
    cout<<"4. Stop Changing"<<endl;
    cout<<endl;
}
unsigned int ask_position()
{
    cout<<"Choose position"<<endl;
    unsigned int i;
    cin>>i;
    return i;
}
void change (vector<Person> &vPerson)
{
    int a;
    bool stop =false;
    while (!stop)
    {
        ask_change();
        cin>>a;
        switch (a) {
        case 1:
        {
            unsigned int i = ask_position();
            cout<<"Enter Name"<<endl;
            string name;
            cin>>name;
            vPerson[i].setName(name);
        }
            break;
        case 2:
        {
            unsigned int i = ask_position();
            cout<<"Enter Second Name"<<endl;
            string second_name;
            cin>>second_name;
            vPerson[i].setSecond_Name(second_name);
        }

            break;
        case 3:
        {
            unsigned int i = ask_position();
            cout<<"Enter Number"<<endl;
            string number;
            cin>>number;
            vPerson[i].setNumber(number);
        }
            break;
        case 4:
            stop=true;
            break;
        default:
            cout<<"Wrong action"<<endl;
            break;
        }
    }
}

Person add_contact()
{
    string name;
    string second_name;
    string number;
    cout<<"Enter Name"<<endl;
    cin>>name;
    cout<<"Enter Second Name"<<endl;
    cin>>second_name;
    cout<<"Enter Number"<<endl;
    cin>>number;
    Person P(name,second_name,number);
    return P;
}

int main(int argc, char *argv[])
{
    string path = argv[1];
    vector<Person> vPerson;
    fstream F;
    F.open(path);
    if(F)
    {
        while(!F.eof())
        {
            string name;
            string second_name;
            string number;
            F>>name;
            F>>second_name;
            F>>number;
            Person P(name,second_name,number);
            vPerson.push_back(P);
        }
    }
    else cout <<"Error reading"<< endl;
    F.close();
    bool save =false;
    while (!save)
    {
        int a;
        write_book(vPerson);
        ask_action();
        cin>>a;
        switch (a) {
        case 1:
        {
            Person P(" "," "," ");
            P=add_contact();
            vPerson.push_back(P);
            cout<<endl;
        }
            break;
        case 2:
            change(vPerson);
            cout<<endl;
            break;
        case 3:
        {
            unsigned int i = ask_position();
            vPerson.erase(vPerson.begin()+i);
            cout<<endl;
        }
            break;
        case 4:
            save=true;
            cout<<endl;
            break;
        default:
            cout<<"Wrong action"<<endl;
            break;
        }
    }
    F.open(path, fstream::out);
    if (F)
        for(int i=0;i<vPerson.size();i++)
        {
            string const &name=vPerson[i].getName();
            string const &second_name=vPerson[i].getSecond_Name();
            string const &number=vPerson[i].getNumber();
            F<<name<<" "<<second_name<<" "<<number;
            if (i!=vPerson.size()-1)
            {
                F<<endl;
            }
        }
    else cout <<"Error writing"<< endl;
    F.close();
    cout<<"Book is saved"<<endl;

    return 0;
}
