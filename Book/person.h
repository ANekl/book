#ifndef PERSON_H
#define PERSON_H
#include <iostream>
using namespace std;

class Person
{
private:
    string name;
    string second_name;
    string number;

public:
    Person();
    Person(string val_name, string val_second_name, string val_number);
    string getName();
    void setName(string value_name);
    string getSecond_Name();
    void setSecond_Name(string value_second_name);
    string getNumber();
    void setNumber(string value_number);
};

#endif // PERSON_H
